#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(readlink -f $(dirname "$0"))"

OPENAL_VERSION="1.15.1"
OPENAL_SOURCE_DIR="openal-soft-$OPENAL_VERSION"

FREEALUT_VERSION="1.1.0"
FREEALUT_SOURCE_DIR="freealut-$FREEALUT_VERSION"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)"

build=${AUTOBUILD_BUILD_ID:=0}
echo "${OPENAL_VERSION}-${FREEALUT_VERSION}.${build}" > "${stage}/VERSION.txt"

build_linux()
{
       # Prefer gcc-4.6 if available.
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            export CC=/usr/bin/gcc-4.6
            export CXX=/usr/bin/g++-4.6
        fi
        # Default target to 32 or 64-bit based on caller.
        opts="${TARGET_OPTS:--m$1}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        mkdir -p openal
        pushd openal
            cmake ../../$OPENAL_SOURCE_DIR -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS=" -m$1 -msse2 -mfpmath=sse -pipe" 
            make -j4
        popd

        mkdir -p "$stage/lib/release"
        cp  -a  "$stage"/openal/libopenal.so* "$stage"/lib/release

        mkdir -p freealut
        pushd freealut
            cmake ../../$FREEALUT_SOURCE_DIR -DCMAKE_C_FLAGS=" -m$1 -msse2 -mfpmath=sse -pipe"  \
                -DOPENAL_LIB_DIR="$stage/openal" -DOPENAL_INCLUDE_DIR="$TOP/$OPENAL_SOURCE_DIR/include"
            make
            cp -a libalut.so* "$stage"/lib/release
        popd

        rm -rf openal/
        rm -rf freealut/
}

case "$AUTOBUILD_PLATFORM" in
    "windows")
        build_sln "OpenAL.sln" "Debug|Win32" "OpenAL32"
        mkdir -p "$stage/lib"
        mv Debug "$stage/lib/debug"

        build_sln "OpenAL.sln" "Release|Win32" "OpenAL32"
        mv Release "$stage/lib/release"
        
        pushd "$TOP/$FREEALUT_SOURCE_DIR/admin/VisualStudioDotNET"
            build_sln "alut.sln" "Debug|Win32" "alut"
            build_sln "alut.sln" "Release|Win32" "alut"
            
            cp -a "alut/Debug/alut.dll" "$stage/lib/debug"
            cp -a "alut/Debug/alut.lib" "$stage/lib/debug"
            cp -a "alut/Release/alut.dll" "$stage/lib/release"            
            cp -a "alut/Release/alut.lib" "$stage/lib/release"            
        popd
    ;;
    "linux")
# Repackage to a working version there is a problem when built on gcc-4.6.4 or 4.9
# The woring version was build to gcc-4.6.3 which isn't set up for autobuild 1.3
#       build_linux 32
	cd ..
        pwd
    	tar -xvf openal-1.15.1-1.1.0-linux-20130811.tar.bz2 -C ${stage}
    ;;

    "linux64")
# Repackage to a working version there is a problem when built on gcc-4.6.4 or 4.9
# The woring version was build to gcc-4.6.3 which isn't set up for autobuild 1.3
#       build_linux 64
	cd ..
        pwd
    	tar -xvf openal-1.15.1-1.1.0-linux64-20130329.tar.bz2 -C ${stage}
    ;;

esac

cp -r "$TOP/$OPENAL_SOURCE_DIR/include" "$stage"
cp -r "$TOP/$FREEALUT_SOURCE_DIR/include" "$stage"

mkdir -p "$stage/LICENSES"
cp "$TOP/$OPENAL_SOURCE_DIR/COPYING" "$stage/LICENSES/openal.txt"
cp "$TOP/$FREEALUT_SOURCE_DIR/COPYING" "$stage/LICENSES/freealut.txt"

# README_DIR="$stage/autobuild-bits"
# README_FILE="$README_DIR/README-Version-3p-openal"
# mkdir -p $README_DIR
# cat $TOP/.hg/hgrc|grep default |sed  -e "s/default = ssh:\/\/hg@/https:\/\//" > $README_FILE
# echo "Commit $(hg id -i)" >> $README_FILE

pass

